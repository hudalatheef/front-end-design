import 'package:courses/pages/InstructorProfile.dart';
import 'package:courses/pages/VideoProfile.dart';
// import 'package:courses/pages/models/post_page.dart';
import 'package:courses/pages/subcourse.dart';
import 'package:courses/pages/webinar.dart';
// import 'package:courses/pages/webinar.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey[200],
      child: ListView(
        children: [
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: () {}),
              SizedBox(
                width: 50,
              ),
              Column(
                children: [
                  Row(
                    children: [
                      Text(
                        'Digital Marketing',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            height: 1,
                            fontSize: 30,
                            color: Color(0xff145d7c)),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 20.0,
              ),
            ]),
            margin: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
            height: 50,
            child: new TextField(
              // controller: _controller,
              decoration: InputDecoration(
                suffixIcon: Padding(
                  padding: const EdgeInsets.only(
                      right: 4.0, top: 2, bottom: 2, left: 2.0),
                  child: SizedBox(
                    width: 64.0,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(60))),
                        color: Color(0xffeef1f3),
                        onPressed: () {},
                        child: Center(
                          child: Icon(
                            Icons.search,
                            size: 32,
                            color: Colors.black,
                          ),
                        )),
                  ),
                ),
                filled: true,
                fillColor: Colors.white,
                hintText: 'Search ',
                hintStyle: TextStyle(
                    fontSize: 18,
                    color: Colors.black45,
                    fontWeight: FontWeight.w600),
                contentPadding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                'DESCRIPTION',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 05,
              ),
              Container(
                height: 30,
                child: SingleChildScrollView(
                  child: Text(
                    'jggduahofhofh kjhshgjsjgnsllgkjsklngkns kjsgbhnnsgjnlklks kjnsnklkdlkjpjs,jk nijnknngih',
                    style: TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                ),
              ),
            ]),
          ),
          SizedBox(
            height: 30,
          ),
          // ignore: missing_required_param
          ProfileMenu(
            text: 'SEO',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),
          // ignore: missing_required_param
          ProfileMenu(
            text: 'Social Media Marketing',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),
          // ignore: missing_required_param
          ProfileMenu(
              text: 'Social Media',
              press: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InstructorProfile()));
              }),
          // ignore: missing_required_param
          ProfileMenu(
            text: 'Ecommerce SEO',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => VideoProfile()));
            },
          ),
          // ignore: missing_required_param
          ProfileMenu(
            text: 'PPC Advertisement',
            press: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Webinar()));
            },
          ),
          // ignore: missing_required_param
          ProfileMenu(
              text: 'Lead Generation',
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Subcourse()));
              }),
          // ignore: missing_required_param
          ProfileMenu(
            text: 'ORM',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),

          SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }
}

class ProfileMenu extends StatelessWidget {
  const ProfileMenu({
    Key key,
    @required this.text,
    this.icon,
    @required this.press,
  }) : super(key: key);

  final String text, icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      child: GestureDetector(
        onTap: press,
        child: Container(
          height: 50.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                const Color(0xff33c0ba),
                const Color(0xff145d7c),
              ],
              stops: [0.0, 1.0],
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    text,
                    style:
                        TextStyle(height: 1, fontSize: 20, color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
