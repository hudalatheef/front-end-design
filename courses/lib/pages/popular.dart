// import 'package:flutter/material.dart';

// class PopularVideos extends StatefulWidget {
//   @override
//   _PopularVideosState createState() => _PopularVideosState();
// }

// class _PopularVideosState extends State<PopularVideos> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         child: CustomAppBar(title: 'Digital Marketing'),
//         preferredSize: Size.fromHeight(60.0),
//       ),
//       body: Container(
//         child: Center(
//           child: GridView.count(
//             // Create a grid with 2 columns. If you change the scrollDirection to
//             // horizontal, this produces 2 rows.
//             crossAxisCount: 2,
//             // Generate 100 widgets that display their index in the List.
//             children: List.generate(20, (index) {
//               return Center(
//                 child: Card(
//                   child: Container(
//                     height: 175.0,
//                     width: 160.0,
//                     color: Colors.green,
//                     child: Text(
//                       'Item $index',
//                       style: Theme.of(context).textTheme.headline5,
//                     ),
//                   ),
//                 ),
//               );
//             }),
//           ),
//         ),
//       ),
//       bottomNavigationBar: Container(
//         padding: EdgeInsets.symmetric(vertical: 14),
//         decoration: BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.only(
//             topLeft: Radius.circular(40),
//             topRight: Radius.circular(40),
//           ),
//           boxShadow: [
//             BoxShadow(
//               offset: Offset(0, -15),
//               blurRadius: 20,
//               color: Color(0xFFDADADA).withOpacity(0.15),
//             ),
//           ],
//         ),
//         child: SafeArea(
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               SizedBox(
//                 width: 0.01,
//               ),
//               IconButton(icon: Icon(Icons.home), onPressed: () {}),
//               IconButton(icon: Icon(Icons.bookmark), onPressed: () {}),
//               IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
//               IconButton(icon: Icon(Icons.play_arrow), onPressed: () {}),
//               SizedBox(
//                 width: 0.01,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
import 'package:flutter/material.dart';
import 'package:courses/widgets/custom_appbar.dart';

/// The base class for the different types of items the list can contain.
abstract class ListItem {
  /// The title line to show in a list item.
  Widget buildTitle(BuildContext context);
}

/// A ListItem that contains data to display a heading.
class HeadingItem implements ListItem {
  Widget buildTitle(BuildContext context) {
    return Text(
      "Popular videos",
      style: TextStyle(
          color: Colors.black,
          fontSize: 18,
          letterSpacing: 0,
          fontWeight: FontWeight.w700),
    );
  }

  Widget buildSubtitle(BuildContext context) => null;
}

/// A ListItem that contains data to display a Single.
class SingleItem implements ListItem {
  Widget buildTitle(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {},
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                image: DecorationImage(
                    image: AssetImage('assets/4.jpg'), fit: BoxFit.cover),
              ),
              height: 210.0,
              width: 150.0,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {},
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                image: DecorationImage(
                    image: AssetImage('assets/4.jpg'), fit: BoxFit.cover),
              ),
              height: 210.0,
              width: 150.0,
            ),
          ),
        ),
      ],
    );
  }
}

final items = List<ListItem>.generate(
  20,
  (i) => i == 0 ? HeadingItem() : SingleItem(),
);

class PopularVideos extends StatefulWidget {
  @override
  PopularVideosState createState() => PopularVideosState();
}

class PopularVideosState extends State<PopularVideos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: CustomAppBar(title: 'Digital Marketing'),
        preferredSize: Size.fromHeight(60.0),
      ),
      body: ListView.builder(
        // Let the ListView know how many items it needs to build.
        itemCount: items.length,
        // Provide a builder function. This is where the magic happens.
        // Convert each item into a widget based on the type of item it is.
        itemBuilder: (context, index) {
          final item = items[index];

          return ListTile(
            title: item.buildTitle(context),
          );
        },
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 14),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            ),
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 0.01,
              ),
              IconButton(icon: Icon(Icons.home), onPressed: () {}),
              IconButton(icon: Icon(Icons.bookmark), onPressed: () {}),
              IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
              IconButton(icon: Icon(Icons.play_arrow), onPressed: () {}),
              SizedBox(
                width: 0.01,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
